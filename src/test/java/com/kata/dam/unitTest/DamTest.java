package com.kata.dam.unitTest;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.kata.dam.DamGame;


public class DamTest
{
	DamGame damGame;
	String secondePlayer;
	String firstPlayer;
	
	@Before
	public void initTheGame()
	{
		firstPlayer = "player1Name:x";
		secondePlayer = "player2Name:o";
		damGame = new DamGame(firstPlayer, secondePlayer);
	}

	@Test
	public void shouldPrintTheInitialStateOfTheBoard()
	{
		assertEquals(firstPlayer+"; "+secondePlayer+"\n\n"
						+"   1 2 3 4 5 6 7 8 \n"
						+"   _______________ \n"
      				+"A |▉|x|▉|x|▉|x|▉|x|\n"
      				+"B |x|▉|x|▉|x|▉|x|▉|\n"
      				+"C |▉|x|▉|x|▉|x|▉|x|\n"
      				+"F | |▉| |▉| |▉| |▉|\n"
      				+"E |▉| |▉| |▉| |▉| |\n"
      				+"H |o|▉|o|▉|o|▉|o|▉|\n"
      				+"I |▉|o|▉|o|▉|o|▉|o|\n"
      				+"J |o|▉|o|▉|o|▉|o|▉|\n"
						+"   ─────────────── ", damGame.printBoard());
	}
	
	@Test //(expected = Exception.IllegalMoveException)
	public void shouldNotAllowOnePlayerToMoveTheOtherPlayerSticks()
	{
		damGame.move(firstPlayer,"G2","F3");
	}
	@Test  //(expected = Exception.NotAllowedPlayerException)
	public void shouldNotAllowAPlayerToPlay2TimesInRow()
	{
		damGame.move(firstPlayer,"D9","E8");
		damGame.move(firstPlayer,"D7","E6");
		
	}
	@Test  //(expected = Exception.NotAllowedPlayerException)
	public void shouldNotAllowTheSecondePlayerToStart()
	{
		damGame.move(secondePlayer,"G2","F3");
	}
	@Test  //(expected = Exception.IllegalMoveException)
	public void shouldNotAllowReturnBack()
	{
		damGame.move(firstPlayer,"D9","E8");
		damGame.move(secondePlayer,"G2","F3");
	}
	
	@Test
	public void shouldAlwaysDoMultipleMoveWhenPossible(){
		// TODO
	}
	
	@Test
	public void shouldAlwaysPrefereSmashingUpTheOpponentPieces(){
		// TODO
	}
	
	
	
	
	
	
	
	
	
	
	

}
